package com.project.movieLibApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseSeeder implements CommandLineRunner {
    private MovieRepository movieRepository;

    @Autowired
    public DatabaseSeeder(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        List<Movie> movies = new ArrayList<>();

        movies.add(new Movie("Truman Show", "1998", "Peter Weir"));
        movies.add(new Movie("Piknik z niedźwidziami", "2017", "Ken Kwapis"));
        movies.add(new Movie("O północy w Paryżu", "2011", "Woody Allen"));
        movieRepository.save(movies);
    }
}
