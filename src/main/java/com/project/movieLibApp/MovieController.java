package com.project.movieLibApp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/movies")
public class MovieController {

	private MovieRepository movieRepository;
	
	@Autowired
	public MovieController(MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}

	@GetMapping(value = "/all")
	public List<Movie> getAll(){
		return movieRepository.findAll();
	}

	@GetMapping("/{id}")
	public Movie getMovie(@PathVariable Long id){
		return movieRepository.findOne(id);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public List<Movie> create(@RequestBody Movie movie){
		movieRepository.save(movie);
		return movieRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/movies/{id}")
	public void updateMovie(@RequestBody Movie movie, @PathVariable Long id){
		movieRepository.save(movie);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value = "/delete/{id}")
	public List<Movie> remove(@PathVariable Long id){
		movieRepository.delete(id);
		return movieRepository.findAll();
	}
	
}

