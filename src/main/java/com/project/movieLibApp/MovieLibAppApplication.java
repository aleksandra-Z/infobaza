package com.project.movieLibApp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieLibAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieLibAppApplication.class, args);
	}
}
